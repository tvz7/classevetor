
import java.util.Arrays;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 20152lbsi120013
 * @param <E>
 */
public class Vetor<E> {

    private final int CAPACIDADE_DEFAULT = 4;

    protected E[] lista;

    protected int capacidadeInicial, incremento;

    protected int numItens;

    public Vetor() {
        lista = (E[]) new Object[CAPACIDADE_DEFAULT];
        incremento = 10;
        capacidadeInicial = CAPACIDADE_DEFAULT;
    }

    public Vetor(int capacidadeInicial) {
        lista = (E[]) new Object[capacidadeInicial];
        this.incremento = 10;
        this.capacidadeInicial = capacidadeInicial;
    }

    public Vetor(int capacidadeInicial, int incremento) {
        lista = (E[]) new Object[capacidadeInicial];
        this.incremento = incremento;
        this.capacidadeInicial = capacidadeInicial;
    }

    public E[] resize(E[] lista) {
        return Arrays.copyOf(lista, lista.length + this.incremento);
    }

    public void imprimeVetor() {
        for (int i = 0; i < lista.length; i++) {
            System.out.print(lista[i] + "| ");
        }
    }

    public void clear() {
        lista = (E[]) new Object[capacidadeInicial];
        for (int i = 0; i < lista.length; i++) {
            lista[i] = null;
        }

        this.numItens = 0;
    }

    public void insertAtEnd(E obj) {

        if ((numItens) == lista.length) {
            this.lista = resize(lista);
        }
        insertAt(numItens, obj);

    }

    public boolean insertAt(int indice, E obj) {
        if (indice > numItens && indice < numItens) {
            return false;
        } else {
            E temp;
            if (!(numItens == lista.length)) {

                for (int i = indice; i < lista.length; i++) {
                    temp = lista[i];
                    lista[i] = obj;
                    obj = temp;
                }
                numItens++;
                return true;

            } else {
                lista = resize(lista);
                for (int i = indice; i < lista.length; i++) {
                    temp = lista[i];
                    lista[i] = obj;
                    obj = temp;
                }
                numItens++;
                return true;
            }
        }
    }

    public boolean insertAtBegin(E obj) {
        insertAt(0, obj);
        return true;
    }

    public E removeFromEnd() {
        int i = numItens - 1;
        E obj = lista[i];
        removeAt(i);
        return obj;
    }

    public E removeAt(int indice) {
        if (indice > numItens && indice < numItens) {
            return null;
        } else {
            E obj = lista[indice];

            for (int i = indice; i < numItens; i++) {
                this.lista[i] = this.lista[i + 1];
            }
            
            lista[numItens - 1] = null;
            numItens--;
            return obj;
        }
    }

    public E removeFromBegin() {
        removeAt(0);
        return lista[0];
    }

    public boolean replace(int indice, E obj) {
        if (indice > lista.length) {
            return false;
        } else {
            lista[indice] = obj;
            return true;
        }
    }

    public E elementAt(int indice) {
        return lista[indice];

    }

    public boolean isEmpty() {
        return this.numItens <= 0;

    }

    public int size() {
        return this.numItens;
    }

    // --- Inner Class Iterator --- 
    public class MyIterator<E> implements Iterator {

        private int index;

        public MyIterator() {
            index = 0;
        }

        @Override
        public boolean hasNext() {
            return index < lista.length;
        }

        @Override
        public Object next() {
            int pos = index++;
            return lista[pos];
        }

    }

    public Iterator<E> iterator() {
        return new MyIterator<E>();
    }

}
